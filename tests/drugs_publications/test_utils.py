import unittest
from pyspark.sql import SparkSession, DataFrame
from datetime import date
from drugs_publications.spark.utils import Utils

class TestUtils(unittest.TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        super().setUpClass()
        cls.spark: SparkSession = Utils.init_spark('Test spark.Utils.parse_date')

    def test_parse_date(self):
        input_dates = [('1 January 2022',), ('10 May 2022',), ('20 Aug 2022',), ('30/09/2022',), ('2022-10-31',)]
        invalid_date = '2022_11_01',
        input_dates.append(invalid_date)
        input_df: DataFrame = self.spark.createDataFrame(input_dates, ['date_str'])
        processed_df = input_df.select(Utils.parse_date(input_df['date_str']))
        processed_result = [row['date'] for row in processed_df.collect()]
        expected_result = [date(2022, 1, 1), date(2022, 5, 10), date(2022, 8, 20), date(2022, 9, 30), date(2022, 10, 31), None]
        self.assertCountEqual(processed_result, expected_result)
