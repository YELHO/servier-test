import unittest
from datetime import date
from pyspark.sql import SparkSession, DataFrame
from drugs_publications.spark.utils import Utils
from drugs_publications.spark.sources.io import SourceIO
from drugs_publications.spark.jobs.drugs_pubmed_publications import DrugsPubmedPublications

class TestPubmedPublications(unittest.TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        super().setUpClass()
        cls.spark: SparkSession = Utils.init_spark('Test drugs pubmed publications processing')
        cls.source_io = SourceIO(cls.spark, 'tests/config/sources.json')
        cls._init_test_data()

    @classmethod
    def _init_test_data(cls) -> None:
        """Prepare the test by writing sample drugs and pubmed publications files to disk"""
        drugs = [
            {"drug": 'drug_1', "atccode": 'code_1'},
            {"drug": 'drug_2', "atccode": 'code_2'},
            {"drug": 'drug_unmatched', "atccode": 'code_unmatched'}
        ]
        drugs_df: DataFrame = cls.spark.createDataFrame(drugs)
        cls.source_io.save_df(drugs_df, 'drugs')
        
        pubmed_part_one = [{"id": 'id_1', "title": 'title_1 referencing drug_1', "date": '1 January 2022', "journal": 'journal_1'}]
        pubmed_part_two = [
            {"id": 'id_2', "title": 'title_2 referencing DRUG_2', "date": '02/01/2022', "journal": 'journal_2'},
            {"id": 'id_unmatched', "title": 'title_unmatched', "date": '03/01/2022', "journal": 'journal_unmatched'}
        ]
        pubmed_df_1: DataFrame = cls.spark.createDataFrame(pubmed_part_one)
        pubmed_df_2: DataFrame = cls.spark.createDataFrame(pubmed_part_two)
        cls.source_io.save_df(pubmed_df_1, 'pubmedPartOne')
        cls.source_io.save_df(pubmed_df_2, 'pubmedPartTwo')


    def test_drugs_pubmed_publications(self):
        processed_result_df = DrugsPubmedPublications().process(self.source_io)
        processed_result_dict = [row.asDict() for row in processed_result_df.collect()]
        expected_result_dict = [
            {"pubmed_id": 'id_1', "pubmed_title": 'title_1 referencing drug_1', "date": date(2022, 1, 1), "journal": 'journal_1', "drug": 'drug_1', "atccode": 'code_1'},
            {"pubmed_id": 'id_2', "pubmed_title": 'title_2 referencing DRUG_2', "date": date(2022, 1, 2), "journal": 'journal_2', "drug": 'drug_2', "atccode": 'code_2'}
        ]
        self.assertCountEqual(processed_result_dict, expected_result_dict)
