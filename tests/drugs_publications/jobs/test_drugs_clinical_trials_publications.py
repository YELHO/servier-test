import unittest
from datetime import date
from pyspark.sql import SparkSession, DataFrame
from drugs_publications.spark.utils import Utils
from drugs_publications.spark.sources.io import SourceIO
from drugs_publications.spark.jobs.drugs_clinical_trials_publications import DrugsClinicalTrialsPublications

class TestClinicalTrialsPublications(unittest.TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        super().setUpClass()
        cls.spark: SparkSession = Utils.init_spark('Test drugs clinical trials publications processing')
        cls.source_io = SourceIO(cls.spark, 'tests/config/sources.json')
        cls._init_test_data()

    @classmethod
    def _init_test_data(cls) -> None:
        """Prepare the test by writing sample drugs and clinical trials publications files to disk"""
        drugs = [
            {"drug": 'drug_1', "atccode": 'code_1'},
            {"drug": 'drug_2', "atccode": 'code_2'},
            {"drug": 'drug_unmatched', "atccode": 'code_unmatched'}
        ]
        drugs_df: DataFrame = cls.spark.createDataFrame(drugs)
        cls.source_io.save_df(drugs_df, 'drugs')
        
        clinical_trials = [
            {"id": 'id_1', "scientific_title": 'title_1 referencing drug_1', "date": '1 January 2022', "journal": 'journal_1'},
            {"id": 'id_2', "scientific_title": 'title_2 referencing DRUG_2', "date": '02/01/2022', "journal": 'journal_2'},
            {"id": 'id_unmatched', "scientific_title": 'title_unmatched', "date": '03/01/2022', "journal": 'journal_unmatched'}
        ]
        clinical_trials_df: DataFrame = cls.spark.createDataFrame(clinical_trials)
        cls.source_io.save_df(clinical_trials_df, 'clinicalTrials')

    def test_drugs_clinical_trials_publications(self):
        processed_result_df = DrugsClinicalTrialsPublications().process(self.source_io)
        processed_result_dict = [row.asDict() for row in processed_result_df.collect()]
        expected_result_dict = [
            {"clinical_trial_id": 'id_1', "clinical_trial_title": 'title_1 referencing drug_1', "date": date(2022, 1, 1), "journal": 'journal_1', "drug": 'drug_1', "atccode": 'code_1'},
            {"clinical_trial_id": 'id_2', "clinical_trial_title": 'title_2 referencing DRUG_2', "date": date(2022, 1, 2), "journal": 'journal_2', "drug": 'drug_2', "atccode": 'code_2'}
        ]
        self.assertCountEqual(processed_result_dict, expected_result_dict)
