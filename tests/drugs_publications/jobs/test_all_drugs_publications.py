import unittest
from datetime import date
from pyspark.sql import SparkSession, DataFrame
from drugs_publications.spark.utils import Utils
from drugs_publications.spark.sources.io import SourceIO
from drugs_publications.spark.jobs.all_drugs_publications import AllDrugsPublications

class TestAllDrugsPublications(unittest.TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        super().setUpClass()
        cls.spark: SparkSession = Utils.init_spark('Test all drugs publications processing')
        cls.source_io = SourceIO(cls.spark, 'tests/config/sources.json')
        cls._init_test_data()

    @classmethod
    def _init_test_data(cls) -> None:
        """Prepare the test by writing sample pubmed and clinical trials publications files to disk.
        The 2 datasets will each have a single matched item (prefixed with matching_),
        and 3 other unmatched items (unmatched date, journal or drug).
        Thus, the resulting dataframe will have 7 rows:
            - One for the matched pubmed and clinical trial where all columns are set
            - 3 unmatched pubmed rows
            - 3 unmatched clinical trials rows
        """
        cls.matched_pubmed = {
            "pubmed_id": 'id_matching_pubmed', "pubmed_title": 'title of matching pubmed referencing matching_drug',
            "date": date(2022, 1, 1), "journal": 'matching_journal', "drug": 'matching_drug', "atccode": 'matching_code'
        }
        cls.pubmed_unmatched_date = dict(cls.matched_pubmed, date=date(2022, 1, 2))
        cls.pubmed_unmatched_journal = dict(cls.matched_pubmed, journal='pubmed unmatched journal')
        cls.pubmed_unmatched_drug = dict(cls.matched_pubmed, drug='pubmed_unmatched_drug', atccode='pubmed_unmatched_code')
        pubmed_publications_df: DataFrame = cls.spark.createDataFrame([cls.matched_pubmed, cls.pubmed_unmatched_date, cls.pubmed_unmatched_journal, cls.pubmed_unmatched_drug])
        cls.source_io.save_df(pubmed_publications_df, 'drugsPubmedPublications')
        
        cls.matched_clinical_trial = {
            "clinical_trial_id": 'id_matching_clinical_trial', "clinical_trial_title": 'title of matching clinical trial referencing matching_drug',
            "date": date(2022, 1, 1), "journal": 'matching_journal', "drug": 'matching_drug', "atccode": 'matching_code'
        }
        cls.clinical_trial_unmatched_date = dict(cls.matched_clinical_trial, date=date(2022, 1, 3))
        cls.clinical_trial_unmatched_journal = dict(cls.matched_clinical_trial, journal='clinical trial unmatched journal')
        cls.clinical_trial_unmatched_drug = dict(cls.matched_clinical_trial, drug='clinical_trial_unmatched_drug', atccode='clinical_trial_unmatched_code')
        clinical_trials_publications_df: DataFrame = cls.spark.createDataFrame([cls.matched_clinical_trial, cls.clinical_trial_unmatched_date, cls.clinical_trial_unmatched_journal, cls.clinical_trial_unmatched_drug])
        cls.source_io.save_df(clinical_trials_publications_df, 'drugsClinicalTrialsPublications')

    def test_all_drugs_publications(self):
        pubmed_null_fields = {"pubmed_id": None, "pubmed_title": None}
        clinical_trial_null_fields = {"clinical_trial_id": None, "clinical_trial_title": None}
        processed_result_df = AllDrugsPublications().process(self.source_io)
        processed_result_dict = [row.asDict() for row in processed_result_df.collect()]
        expected_result_dict = [
            dict(self.matched_pubmed, **self.matched_clinical_trial),
            dict(self.pubmed_unmatched_date, **clinical_trial_null_fields),
            dict(self.pubmed_unmatched_journal, **clinical_trial_null_fields),
            dict(self.pubmed_unmatched_drug, **clinical_trial_null_fields),
            dict(self.clinical_trial_unmatched_date, **pubmed_null_fields),
            dict(self.clinical_trial_unmatched_journal, **pubmed_null_fields),
            dict(self.clinical_trial_unmatched_drug, **pubmed_null_fields)
        ]
        self.assertCountEqual(processed_result_dict, expected_result_dict)