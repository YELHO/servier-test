from drugs_publications.args_parse_utils import parse_sources_json_config_path_arg
from drugs_publications.spark.utils import Utils
from drugs_publications.spark.jobs import all_drugs_publications, drugs_clinical_trials_publications, drugs_pubmed_publications

if __name__ == "__main__":
    # For each drug, date and journal, process the corresponding pubmed and/or clinical trial publications.
    # The pipeline is composed of 3 different tasks executed sequentially.
    # The last called module ('all_drugs_publications') requires the outcomes of the 2 prior modules. 
    # Each task is implemented by an independent module that can be executed on its own as a python script.
    # This allows for higher modularity as these modules can be reused individually in other projects or pipelines.
    # A module (e.g drugs_pubmed_publications) can be ran individually by the command: python -m spark.jobs.drugs_pubmed_publications config/sources.json
    sources_json_config_path = parse_sources_json_config_path_arg()
    spark = Utils.init_spark('Drugs all publications')
    drugs_pubmed_publications.main(sources_json_config_path, spark)
    drugs_clinical_trials_publications.main(sources_json_config_path, spark)
    all_drugs_publications.main(sources_json_config_path, spark)
    spark.stop()