from pyspark.sql import SparkSession, Column
from pyspark.sql import functions as F

class Utils:
    
    @staticmethod
    def init_spark(app_name: str) -> SparkSession:
        return SparkSession.builder.appName(app_name).getOrCreate()

    @staticmethod
    def parse_date(str_date: Column, alias: str = 'date') -> Column:
        """A date parser supporting differents date formats"""
        return F\
            .when(str_date.rlike('\d{1,2} [A-Za-z]{3} \d{4}'), F.to_date(str_date, 'd MMM yyyy'))\
            .when(str_date.rlike('\d{1,2} [A-Za-z]{4,} \d{4}'), F.to_date(str_date, 'd MMMM yyyy'))\
            .when(str_date.rlike('\d{1,2}/\d{1,2}/\d{4}'), F.to_date(str_date, 'dd/MM/yyyy'))\
            .otherwise(F.to_date(str_date))\
            .alias(alias)
