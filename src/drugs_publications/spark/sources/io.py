import json
from pyspark.sql import SparkSession, DataFrame
from .source import Source

class SourceIO:
    """Load sources config from json and stores them in a dict.
    Supply methods to read/write a source from/to disk given its name.
    The class is implemented as a singleton.
    """
    _instance = None # Class variable that holds the unique class instance

    def __new__(cls, spark: SparkSession, sources_json_path: str):
        """Redefine '__new__' method to instantiate the class only once."""
        if cls._instance is None:
            cls._instance = super().__new__(cls)
            cls._init_instance(spark, sources_json_path)   
        return cls._instance

    @classmethod
    def _init_instance(cls, spark: SparkSession, sources_json_path: str):
        with open(sources_json_path) as sources_json_file:
            sources_json = json.load(sources_json_file)
        cls._instance.sources: dict[str, Source] = {}
        cls._instance.spark = spark
        for source_name, source_config in sources_json['sources'].items():
            cls._instance.sources[source_name] = Source(source_name, source_config)

    def load_df(self, source_name: str) -> DataFrame:
        source = self._instance.sources[source_name]
        return self._instance.spark.read.format(source.format).options(**source.read_options).load(source.path)

    def save_df(self, df: DataFrame, source_name: str) -> None:
        source = self._instance.sources[source_name]
        df.write.save(source.path, source.format, source.write_mode, None, **source.write_options)
