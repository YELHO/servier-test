class Source:
    """Encapsulate source config needed to read/write data with spark"""
    def __init__(self, source_name: str, source_config: dict) -> None:
        self.name = source_name
        # path, format & write options are mandatory
        self.path: str = source_config['path']
        self.format: str = source_config['format']
        # read & write options are optional
        self.read_options: dict = source_config.get('readOptions', {})
        self.write_options: dict = source_config.get('writeOptions', {})
        self.write_mode: str = source_config.get('writeMode', None)
