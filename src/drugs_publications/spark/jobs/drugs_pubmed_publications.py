from typing import Optional
from pyspark.sql import SparkSession, DataFrame, functions as F
from drugs_publications.args_parse_utils import parse_sources_json_config_path_arg
from drugs_publications.spark.sources.io import SourceIO
from drugs_publications.spark.utils import Utils

class DrugsPubmedPublications:
    """Process drugs pubmed publications"""
    def __init__(self) -> None:
        pass
    
    def process(self, source_io: SourceIO) -> DataFrame:
        """Process a join between drugs and clinical trials publications
        based on the condition : lower('title').contains(lower('drug')).
        Dates are parsed using 'the Utils.parse_date' helper function.
        The resulting dataframe will have the following schema:
        root
        |-- atccode: string (nullable = true)
        |-- date: date (nullable = true)
        |-- drug: string (nullable = true)
        |-- journal: string (nullable = true)
        |-- pubmed_id: string (nullable = true)
        |-- pubmed_title: string (nullable = true)
        """
        pubmed_df = source_io.load_df("pubmedPartOne")\
            .unionByName(source_io.load_df("pubmedPartTwo"))\
            .withColumn('date', Utils.parse_date(F.col('date')))
        drugs_df = source_io.load_df('drugs')
        return\
            pubmed_df\
                .join(drugs_df, F.lower('title').contains(F.lower('drug')))\
                .withColumnRenamed('id', 'pubmed_id')\
                .withColumnRenamed('title', 'pubmed_title')


def main(sources_json_path: str, spark_session: Optional[SparkSession] = None):
    """If run as a script: init spark session, process then save outcome to disk."""
    spark = Utils.init_spark('Drugs pubmed publications') if spark_session is None else spark_session
    source_io = SourceIO(spark, sources_json_path)
    drugs_pubmed_publications_df = DrugsPubmedPublications().process(source_io)
    source_io.save_df(drugs_pubmed_publications_df, 'drugsPubmedPublications')

if __name__ == "__main__":
    main(parse_sources_json_config_path_arg())