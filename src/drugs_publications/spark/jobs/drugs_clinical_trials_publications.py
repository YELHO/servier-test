from typing import Optional
from pyspark.sql import SparkSession, DataFrame, functions as F
from drugs_publications.args_parse_utils import parse_sources_json_config_path_arg
from drugs_publications.spark.sources.io import SourceIO
from drugs_publications.spark.utils import Utils

class DrugsClinicalTrialsPublications:
    """Process drugs clinical trials publications"""
    def __init__(self) -> None:
        pass

    def process(self, source_io: SourceIO) -> DataFrame:
        """Process a join between drugs and clinical trials publications
        based on the condition : lower('scientific_title').contains(lower('drug')).
        The resulting dataframe will have the following schema:
        root
        |-- atccode: string (nullable = true)
        |-- clinical_trial_id: string (nullable = true)
        |-- clinical_trial_title: string (nullable = true)
        |-- date: date (nullable = true)
        |-- drug: string (nullable = true)
        |-- journal: string (nullable = true)
        """
        clinical_trials_df = source_io.load_df("clinicalTrials")\
            .withColumn('date', Utils.parse_date(F.col('date')))
        drugs_df = source_io.load_df('drugs')
        return\
            clinical_trials_df\
                .join(drugs_df, F.lower('scientific_title').contains(F.lower('drug')))\
                .withColumnRenamed('id', 'clinical_trial_id')\
                .withColumnRenamed('scientific_title', 'clinical_trial_title')


def main(spark_session: Optional[SparkSession] = None, sources_json_path: Optional[str] = 'config/sources.json'):
    """If run as a script: init spark session, process then save outcome to disk."""
    spark = Utils.init_spark('Drugs clinical trials publications') if spark_session is None else spark_session
    source_io = SourceIO(spark, sources_json_path)
    drugs_clinical_trials_publications_df = DrugsClinicalTrialsPublications().process(source_io)
    source_io.save_df(drugs_clinical_trials_publications_df, 'drugsClinicalTrialsPublications')

if __name__ == "__main__":
    main(parse_sources_json_config_path_arg())
