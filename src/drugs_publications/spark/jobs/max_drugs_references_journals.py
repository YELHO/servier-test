from typing import Optional
from pyspark.sql import SparkSession, functions as F
from drugs_publications.args_parse_utils import parse_sources_json_config_path_arg
from drugs_publications.spark.sources.io import SourceIO
from drugs_publications.spark.utils import Utils

class MaxDrugsReferencesJournals:
    "Process journals with maximum number of distinct drugs references"
    def __init__(self) -> None:
        pass

    def process(self, source_io: SourceIO) -> list[str]:
        drugs_all_publications_df = source_io.load_df("drugsAllPublications")
        journals_drugs_count_df = drugs_all_publications_df\
                .groupBy('journal')\
                .agg(F.count_distinct('drug').alias('drugs_count'))
        max_drugs_count_journals_df = journals_drugs_count_df\
            .crossJoin(journals_drugs_count_df.select(F.max('drugs_count').alias('max_drugs_count')))\
            .where('drugs_count = max_drugs_count')\
            .select('journal')
        return [row['journal'] for row in max_drugs_count_journals_df.collect()]


def main(sources_json_path: str, spark_session: Optional[SparkSession] = None):
    """If run as a script: init spark session, process then print outcome to console."""
    spark = Utils.init_spark('Maximum drugs references journals') if spark_session is None else spark_session
    source_io = SourceIO(spark, sources_json_path)
    max_drugs_references_journals = MaxDrugsReferencesJournals().process(source_io)
    print('+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')
    print(f'++ Journals with maximum drugs references are: {max_drugs_references_journals}')
    print('+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')

if __name__ == "__main__":
    main(parse_sources_json_config_path_arg())
            