from typing import Optional
from pyspark.sql import SparkSession, DataFrame, functions as F
from drugs_publications.args_parse_utils import parse_sources_json_config_path_arg
from drugs_publications.spark.sources.io import SourceIO
from drugs_publications.spark.utils import Utils

class AllDrugsPublications:
    """Process the join between pubmed & clinical trials publications, based on drug, journal and date"""
    def __init__(self) -> None:
        pass

    def process(self, source_io: SourceIO) -> DataFrame:
        """Process a full join between pubmed & clinical trials publications
        based on 'drug', 'atccode', 'date' and 'journal' columns.
        The resulting dataframe will have the following schema:
        root
        |-- drug: string (nullable = true)
        |-- atccode: string (nullable = true)
        |-- date: date (nullable = true)
        |-- journal: string (nullable = true)
        |-- pubmed_id: string (nullable = true)
        |-- pubmed_title: string (nullable = true)
        |-- clinical_trial_id: string (nullable = true)
        |-- clinical_trial_title: string (nullable = true)
        """
        drugs_pubmed_publications_df = source_io\
            .load_df('drugsPubmedPublications')\
            .withColumn('date', Utils.parse_date(F.col('date')))
        drugs_clinical_trials_publications_df = source_io\
            .load_df('drugsClinicalTrialsPublications')\
            .withColumn('date', Utils.parse_date(F.col('date')))
        return\
            drugs_pubmed_publications_df\
                .join(drugs_clinical_trials_publications_df,['drug', 'atccode', 'date', 'journal'], 'full_outer')


def main(sources_json_path: str, spark_session: Optional[SparkSession] = None):
    """If run as a script: init spark session, process then save outcome to disk."""
    spark = Utils.init_spark('Drugs all publications') if spark_session is None else spark_session
    source_io = SourceIO(spark, sources_json_path)
    drugs_all_publications_df = AllDrugsPublications().process(source_io)
    source_io.save_df(drugs_all_publications_df, 'drugsAllPublications')

if __name__ == "__main__":
    main(parse_sources_json_config_path_arg())
