import argparse, os

def parse_sources_json_config_path_arg() -> str:
    parser = argparse.ArgumentParser()
    parser.add_argument('sources_json_config_path', help='path to sources json configuration file')
    args = parser.parse_args()
    if not os.path.isfile(args.sources_json_config_path):
        raise ValueError(f'sources json configuration file ${args.sources_json_config_path} not found')
    return args.sources_json_config_path