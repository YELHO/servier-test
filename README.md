# servier-test

## Description
This project simulates a data pipeline using pyspark to process drugs references in different publications.

The pipeline is composed of 3 independent tasks executed sequentially.

Each task is implemented by an independent module that can be executed on its own as a python script.

This allows for higher modularity as these modules can be reused individually in other projects or pipelines.

The project uses src layout. All code goes under sr/drugs_publications package.

Build configuration is defined in 'pyproject.toml' file.

## Installation
The project has been developed on Windows using python 3.10.
The project uses 'src layout' and is run/tested as an installed module. It needs therefore a regular or editable install before use:
```shell
python -m venv .venv
.venv\Scripts\activate
pip install -e .
```

## Usage of the main pipeline
The main pipeline can be ran using the module 'drugs_publications.complete_pipeline'.
For each drug, date and journal, the module processes the corresponding pubmed and/or clinical trial publications.
The module requires a single argument corresponding to the path of a json file defining the configuration of sources (see 'Input and output data' section for more details on sources).
The json file used for my tests is under config/sources.json:
```shell
python -m drugs_publications.complete_pipeline config/sources.json
```
According to definitions in 'config/sources.json', consumed input files are under 'input_data/' and the generated files go under 'output_data/'.

The `drugs_publications.complete_pipeline` module is implemented using 3 different modules under `drugs_publications.spark.jobs` package, executed sequentially:
1. `drugs_pubmed_publications`: process drugs references in pubmed publications.
2. `drugs_clinical_trials_publications`: process drugs references in clinical trials publications.
3. `all_drugs_publications`: process drugs references in all kind of publications by combining the outcomes of the 2 previous modules **(it requires the outcomes of the 2 previous modules).**

Each of these modules is independent and can be ran on its own as a python script, in which case a spark session is started, and the processed outcome is written to disk. In the same way, each of these modules requires a sources json configuration file as an argument, e.g:
```shell
python -m drugs_publications.spark.jobs.drugs_pubmed_publications config/sources.json
```

## Process journals with maximum number of distinct drugs references
Journals with maximum number of distinct drugs references are processed by the `max_drugs_references_journals` module under `drugs_publications.spark.jobs` package.
It requires the outcome of `all_drugs_publications` module and a sources json configuration file as an argument. It can be ran using the command:
```shell
python -m drugs_publications.spark.jobs.max_drugs_references_journals config/sources.json
```
The outcome of the job is printed to console as follows:
```text
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
++ Journals with maximum drugs references are: ['Psychopharmacology', 'The journal of maternal-fetal & neonatal medicine']
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
```

## Input and output data
Data is read or written to disk using classes under `drugs_publications.spark.sources` package.

`Source` class encapsulates config data needed to read/write a spark input/output: format(csv, json, ...), path(input_data/drugs, ...), read options(delimiter, header, ...), etc.

`SourceIO` class loads all `Sources` config from the json file passed to its constructor, and keeps a dict that maps each `Source` name to its config. `SourceIO` also provides methods to read/write a `Source` from/to disk given its name.

The `Sources` json config used for the main pipeline is under `config\sources.json`. As described in this file, input data (drugs, pubmed and clinical trials) go under folder `input_data\`, and spark processing outcomes of the 3 modules go under foler `output_data\`

## Tests
Unit tests can be ran using the command:
```shell
python -m unittest discover -v
```
### Tests input and output data
Tests have been customized to use `Sources` config under `tests\config\sources.json`.
Tests input and output data go under `tests\input_data` and `tests\output_data` respectively.

## Build
To build sdist and wheel distributions, install and run buil module:
```shell
pip install build
python -m build
```

## Possible improvements for very large datasets
1. Use columnar file format such as `parquet` for input/output files instead of csv or json
2. Think of relevant data partitioning (by date, and/or journal, ...)
3. Resize the spark cluster accordingly

## SQL requests
The SQL requests asked in the second part of the test are implemented in files `request_1.sql` and `request_2.sql` under `sql` folder.

## Gitlab CI
The gitlab CI uses a custom docker image with java 11 and python 3.10 pre-installed.
It runs unit tests and produces source and wheel dirtributions for download (as artifacts) 
