-- Enclose date & transaction in double quotes to not to be taken as specific database key words
SELECT
    trx."date", SUM(prod_price * prod_qty) AS ventes
FROM
    "transaction" AS trx
WHERE
    EXTRACT(YEAR FROM trx."date") = 2019
GROUP BY
    trx."date"
ORDER BY
    trx."date";