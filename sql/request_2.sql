SELECT
    client_id,
    SUM(CASE product_type WHEN 'MEUBLE' THEN ventes ELSE 0 END) AS ventes_meuble,
    SUM(CASE product_type WHEN 'DECO' THEN ventes ELSE 0 END) AS ventes_deco
FROM
    (
        SELECT
            client_id, trx.prod_id, product_type, prod_price * prod_qty AS ventes
        FROM
            "transaction" AS trx
        INNER JOIN
            product_nomenclature AS nomenclature ON trx.prod_id = nomenclature.prod_id
        WHERE
            EXTRACT(YEAR FROM trx."date") = 2019 AND product_type IN ('MEUBLE', 'DECO')
    ) AS trx_meuble_deco
GROUP BY
    client_id;